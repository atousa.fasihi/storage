import mysql.connector

db_conn = mysql.connector.connect(host="acit3855-a2.westeurope.cloudapp.azure.com", user="Tom", password="password1", database="events", auth_plugin='mysql_native_password')
db_cursor = db_conn.cursor()


db_cursor.execute('''
          CREATE TABLE carbon_monoxide
          (id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
           carbon_monoxide_level INTEGER NOT NULL,
           device_id VARCHAR(250) NOT NULL,
           temperature INTEGER NOT NULL,
           timestamp VARCHAR(100) NOT NULL,
           trace_id VARCHAR(250) NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

db_cursor.execute('''
          CREATE TABLE nitrogen_dioxide
          (id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, 
           nitrogen_dioxide_level INTEGER NOT NULL,
           device_id VARCHAR(250) NOT NULL,
           humidity INTEGER NOT NULL,
           timestamp VARCHAR(100) NOT NULL,
           trace_id VARCHAR(250) NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

db_conn.commit()
db_conn.close()
