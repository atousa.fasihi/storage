from sqlalchemy import Column, Integer, String, DateTime
from base import Base
import datetime


class NitrogenDioxide(Base):
    """ nitrogen dioxide """

    __tablename__ = "nitrogen_dioxide"

    id = Column(Integer, primary_key=True)
    nitrogen_dioxide_level = Column(Integer, nullable=False)
    device_id = Column(String(250), nullable=False)
    humidity = Column(Integer, nullable=False)
    timestamp = Column(String(100), nullable=False)
    trace_id = Column(String(250), nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, nitrogen_dioxide_level, device_id, humidity, timestamp, trace_id):
        """ Initializes a nitrogen dioxide reading """
        self.nitrogen_dioxide_level = nitrogen_dioxide_level
        self.device_id = device_id
        self.humidity = humidity
        self.timestamp = timestamp
        self.date_created = datetime.datetime.now() # Sets the date/time record is created
        self.trace_id = trace_id

    def to_dict(self):
        """ Dictionary Representation of a nitrogen dioxide level reading """
        dict = {}
        dict['id'] = self.id
        dict['nitrogen_dioxide_level'] = self.nitrogen_dioxide_level
        dict['device_id'] = self.device_id
        dict['humidity'] = self.humidity
        dict['timestamp'] = self.timestamp
        dict['date_created'] = self.date_created
        dict['trace_id'] = self.trace_id

        return dict
