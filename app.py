import connexion
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from base import Base
from carbon_monoxide import CarbonMonoxide
from nitrogen_dioxide import NitrogenDioxide
import logging.config
import yaml
import json
from pykafka import KafkaClient
from pykafka.common import OffsetType
from threading import Thread
import time
import os

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

# with open('app_conf.yml', 'r') as f:
#    app_config = yaml.safe_load(f.read())

user = app_config['datastore']['user']
password = app_config['datastore']['password']
hostname = app_config['datastore']['hostname']
port = app_config['datastore']['port']
db = app_config['datastore']['db']

create_engine_str = f'mysql+pymysql://{user}:{password}@{hostname}:{port}/{db}'

DB_ENGINE = create_engine(create_engine_str)
Base.metadata.bind = DB_ENGINE
Base.metadata.create_all(DB_ENGINE)
DB_SESSION = sessionmaker(bind=DB_ENGINE)

# with open('log_conf.yml', 'r') as f:
#    log_config = yaml.safe_load(f.read())
#    logging.config.dictConfig(log_config)

# logger = logging.getLogger('basicLogger')

logger.info(f"Connecting to DB. Hostname:{hostname}, Port: {port}")


def get_health():
    """ gets the health of each service and return 200 """
    logger.info("HEALTH CHECK - Return 200")
    logger.info("TESTINGGGGGGGGGGG")
    dict = {"message": "running"}
    return dict, 200


def get_carbon_monoxide_readings(timestamp):
    """ Gets new carbon monoxide readings after the timestamp """
    session = DB_SESSION()

    # start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%d %H:%M:%S.%f")
    # end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%d %H:%M:%S.%f")

    # timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S")

    readings = session.query(CarbonMonoxide).filter(CarbonMonoxide.date_created >= timestamp)
    # readings = session.query(CarbonMonoxide).filter(and_(CarbonMonoxide.date_created >= start_timestamp_datetime, CarbonMonoxide.date_created >= end_timestamp_datetime))
    results_list = []
    for reading in readings:
        results_list.append(reading.to_dict())

    session.close()
    logger.info("Query for carbon monoxide readings after %s returns %d results" % (timestamp, len(results_list)))
    return results_list, 200


def get_nitrogen_dioxide_readings(timestamp):
    """ Gets new nitrogen dioxide readings after the timestamp """
    session = DB_SESSION()

    # start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%d %H:%M:%S.%f")
    # end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%d %H:%M:%S.%f")

    # timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S")

    readings = session.query(NitrogenDioxide).filter(NitrogenDioxide.date_created >= timestamp)
    # readings = session.query(NitrogenDioxide).filter(and_(NitrogenDioxide.date_created >= start_timestamp_datetime, NitrogenDioxide.date_created >= end_timestamp_datetime,))
    results_list = []
    for reading in readings:
        results_list.append(reading.to_dict())

    session.close()
    logger.info("Query for nitrogen dioxide readings after %s returns %d results" % (timestamp, len(results_list)))
    return results_list, 200


def report_carbon_monoxide_reading(body):
    """Receives a carbon monoxide event """
    pass


#    session = DB_SESSION()

#    bp = CarbonMonoxide(body['carbon_monoxide_level'],
#                        body['device_id'],
#                        body['temperature'],
#                        body['timestamp'],
#                        body['trace_id'])

#    session.add(bp)
#    session.commit()
#    session.close()
#    logger.debug("Stored event - carbon monoxide request with a trace id of %s", body['trace_id'])

#    return NoContent, 201


def report_nitrogen_dioxide_reading(body):
    """Receives a nitrogen dioxide event """
    pass


#    session = DB_SESSION()

#    hr = NitrogenDioxide(body['nitrogen_dioxide_level'],
#                         body['device_id'],
#                         body['humidity'],
#                         body['timestamp'],
#                         body['trace_id'])

#    print(body['trace_id'])
#    session.add(hr)
#    session.commit()
#    session.close()
#    logger.debug("Stored event - nitrogen dioxide request with a trace id of %s", body['trace_id'])

#    return NoContent, 201


def process_messages():
    """ Process event messages """
    hostname = "%s:%d" % (app_config["events"]["hostname"], app_config["events"]["port"])
    logger.debug(f'hostname: {hostname}')
    current_retry_num = 0
    max_tries = app_config["events"]["max_tries"]
    time_in_seconds = app_config["events"]["seconds"]

    while current_retry_num < max_tries:
        try:
            logger.info(f"Trying to reconnect to kafka with retry count : {current_retry_num}")
            client = KafkaClient(hosts=hostname)
            topic = client.topics[str.encode(app_config["events"]["topic"])]
            break
        except:
            logger.error(f"Connection to Kafka failed")
            time.sleep(time_in_seconds)
            current_retry_num += 1

    # Create a consume on a consumer group, that only reads new messages
    # (uncommitted messages) when the service re-starts (i.e., it doesn't
    # read all the old messages from the history in the message queue).
    consumer = topic.get_simple_consumer(consumer_group=b'event_group', reset_offset_on_start=False,
                                         auto_offset_reset=OffsetType.LATEST)

    # This is blocking - it will wait for a new message
    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        logger.info("Message: %s" % msg)

        payload = msg["payload"]

        if msg["type"] == "carbon_monoxide":  # Change this to your event type
            # Store the event1 (i.e., the payload) to the DB
            session = DB_SESSION()

            data = CarbonMonoxide(payload['carbon_monoxide_level'],
                                  payload['device_id'],
                                  payload['temperature'],
                                  payload['timestamp'],
                                  payload['trace_id'])

            session.add(data)
            logger.info("TESTINGGGGGGGGGGG")
            session.commit()
            session.close()

        elif msg["type"] == "nitrogen_dioxide":  # Change this to your event type
            # Store the event2 (i.e., the payload) to the DB
            session = DB_SESSION()

            data = NitrogenDioxide(payload['nitrogen_dioxide_level'],
                                   payload['device_id'],
                                   payload['humidity'],
                                   payload['timestamp'],
                                   payload['trace_id'])

            session.add(data)

            session.commit()
            session.close()

        # Commit the new message as being read
        consumer.commit_offsets()


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml", base_path="/storage", strict_validation=True, validate_responses=True)
# app.add_api("openapi.yaml",
#            strict_validation=True,
#            validate_responses=True)

if __name__ == "__main__":
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()
    app.run(port=8090)
