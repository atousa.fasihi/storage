import sqlite3

conn = sqlite3.connect('readings.sqlite')

c = conn.cursor()
c.execute('''
          DROP TABLE carbon_monoxide
          ''')
c.execute('''
          DROP TABLE nitrogen_dioxide
          ''')

conn.commit()
conn.close()
