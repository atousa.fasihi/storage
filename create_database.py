import sqlite3

conn = sqlite3.connect('readings.sqlite')

c = conn.cursor()
c.execute('''
          CREATE TABLE carbon_monoxide
          (id INTEGER PRIMARY KEY ASC, 
           carbon_monoxide_level INTEGER NOT NULL,
           device_id VARCHAR(250) NOT NULL,
           temperature INTEGER NOT NULL,
           timestamp VARCHAR(100) NOT NULL,
           trace_id VARCHAR(250) NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

c.execute('''
          CREATE TABLE nitrogen_dioxide
          (id INTEGER PRIMARY KEY ASC, 
           nitrogen_dioxide_level INTEGER NOT NULL,
           device_id VARCHAR(250) NOT NULL,
           humidity INTEGER NOT NULL,
           timestamp VARCHAR(100) NOT NULL,
           trace_id VARCHAR(250) NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

conn.commit()
conn.close()
